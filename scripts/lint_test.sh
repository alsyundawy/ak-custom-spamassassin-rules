#!/bin/bash

set -e
set -x

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CONFIG="${SCRIPT_DIR}/../93_ak_custom_rules.cf"

BINARY=spamassassin

SA=`which ${BINARY}`

${SA} -C ${CONFIG} --lint
